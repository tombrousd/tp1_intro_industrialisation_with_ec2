provider "aws" {
  region = "eu-west-1" 
}

resource "aws_key_pair" "deployer" {
  key_name   = "ssh_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDcaatxSVK642i2JKtNC85yRdrhfFM9Je+JDu05/vxVMcqV9nERg2TQPtN6q6EXrIBA+5sVobmdxCMC1FSClC2G/4rwuthwP53PeFHNWcL68ZiUGsJ3wW0eJiojbu3KaTDL0NBc+O2Y9CZbIJWOT7XVT2aRt4vPnwbRt6nEmgt7FTDa4irrzuPPYtY8lzdBzjQm4FL6cvAMA7AopQBrsoBZybT904XXMPdA2JL2l2qSDfCrgiuLn1zNpaQRx0665nD8WvwpEmcoWWbM3LKIekVV0XWEWPNam9JeVAIAC7ZKyvopWghEsBKi46G8nkX7Zy2HNG77OqrwCv3j201pcU2rCLXH4tGoEe+nn3l8yjU6Y31q5UjVKrIpa5w1y+j9HFa12Np9YGWMznu/NCDRdL8tlxFfR10duflqyzTtmeJhppzE1XBOIuRtGNXt1jbvMCGe3Pi57mBUs7S67fLqR+3V7kl/sx+WUMuOxrYv6+utbW3sOlewDO2wGhvJhK4CFRM= tombroussard@MacTomB.local"
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_security_group" "default" {
  name        = "my-custom-security-group"
  description = "grpe secu customised"
  vpc_id      = aws_vpc.mainvpc.id

  // Exemple de règle d'entrée pour permettre SSH depuis n'importe où
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Exemple de règle de sortie autorisant tout le trafic sortant
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  ami           = "ami-01dd271720c1ba44f" # Remplacez par l'ID de l'AMI souhaité
  instance_type = "t2.micro" # Remplacez par le type d'instance souhaité

  tags = {
    Name = "EC2-TP1"
  }
}